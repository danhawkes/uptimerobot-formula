#!/usr/bin/env python


def alert_contacts_present(name, api_key, alert_contacts):
    test = __opts__['test']

    ur_alert_contacts = __salt__['uptimerobot.get_alert_contacts'](api_key)
    ur_account_details = __salt__['uptimerobot.get_account_details'](api_key)

    current = {x['friendly_name']: x for x in ur_alert_contacts}
    desired = {x['friendly_name']: x for x in alert_contacts}

    # Uptimerobot automatically adds the account email as an alert contact.
    # This contact can't be removed/modified, so it's silently added to the desired contacts.
    account_email = ur_account_details['account']['email']
    desired.update({account_email: current[account_email]})

    delete, create, changes = _diff(current, desired)

    if not test:
        for friendly_name in delete:
            id = str(current[friendly_name]['id'])
            __salt__['uptimerobot.delete_alert_contact'](api_key, id)

        for friendly_name in create:
            __salt__['uptimerobot.add_alert_contact'](api_key,
                                                      desired[friendly_name])

    ret = {}
    ret['name'] = name
    ret['changes'] = changes
    if test:
        ret['result'] = None if changes else True
        ret['comment'] = 'Alert contacts will be updated' if changes else 'Alert contacts are already in the desired state'
    else:
        ret['result'] = True
        ret['comment'] = 'Alert contacts were updated' if changes else 'Alert contacts were already in the desired state'

    return ret


def monitors_present(name, api_key, monitors):
    test = __opts__['test']

    # Alert contacts, keyed by ID and friendly name
    alert_contacts_by_id = {
        x['id']: x
        for x in __salt__['uptimerobot.get_alert_contacts'](api_key)
    }
    alert_contacts_by_name = {
        x['friendly_name']: x
        for x in __salt__['uptimerobot.get_alert_contacts'](api_key)
    }

    # Monitors, keyed by friendly name
    json_monitors = __salt__['uptimerobot.get_monitors'](api_key)
    for m in json_monitors:
        m['alert_contacts'] = [
            alert_contacts_by_id[ac['id']]['friendly_name']
            for ac in m['alert_contacts']
        ]
    current = {x['friendly_name']: x for x in json_monitors}
    desired = {x['friendly_name']: x for x in monitors}

    delete, create, changes = _diff(current, desired)

    if not test:
        for friendly_name in delete:
            id = current[friendly_name]['id']
            __salt__['uptimerobot.delete_monitor'](api_key, id)

        for friendly_name in create:
            m = desired[friendly_name]
            if m['alert_contacts']:
                m['alert_contacts'] = '-'.join([
                    alert_contacts_by_name[name]['id'] + '_0_0'
                    for name in m['alert_contacts']
                ])
            __salt__['uptimerobot.add_monitor'](api_key, m)

    ret = {}
    ret['name'] = name
    ret['changes'] = changes
    if test:
        ret['result'] = None if changes else True
        ret['comment'] = 'Monitors will be updated' if changes else 'Monitors are already in the desired state'
    else:
        ret['result'] = True
        ret['comment'] = 'Monitors were updated' if changes else 'Monitors were already in the desired state'

    return ret


def _diff(current, desired):
    current_names = set(current.keys())
    desired_names = set(desired.keys())

    # Names of items to create, delete and check for changes
    delete = current_names.difference(desired_names)
    create = desired_names.difference(current_names)
    check = current_names.intersection(desired_names)

    changes = {}

    # Items that are created/deleted outright are logged as 'x -> None' or vice-versa.
    for friendly_name in delete.union(create):
        changes.update({
            friendly_name: {
                'old': current.get(friendly_name),
                'new': desired.get(friendly_name)
            }
        })

    # Items that may have changed are tested key-by-key and logged as such.
    for friendly_name in check:
        cur = current[friendly_name]
        des = desired[friendly_name]
        changed_keys = []
        for key in des.keys():
            if key not in cur or cur[key] != des[key] and set(cur[key]) != set(
                    des[key]):
                changed_keys.append(key)
                # Mechanism for changing items is to delete and recreate.
                delete.add(friendly_name)
                create.add(friendly_name)
        if changed_keys:
            changes.update({
                friendly_name: {
                    'old': {x: cur.get(x)
                            for x in changed_keys},
                    'new': {x: des.get(x)
                            for x in changed_keys}
                }
            })

    return (delete, create, changes)

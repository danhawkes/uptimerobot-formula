#!/usr/bin/env python

import urllib
import urllib2
import json


def _send_request(api_key, endpoint, params={}):
    url = 'https://api.uptimerobot.com/v2/{}'.format(endpoint)
    merged_params = params.copy()
    merged_params.update({'api_key': api_key, 'format': 'json'})
    data = urllib.urlencode(merged_params)
    req = urllib2.Request(url, data)
    response = urllib2.urlopen(req)
    json_response = json.loads(response.read())
    if json_response['stat'] == 'ok':
        return json_response
    else:
        raise RuntimeError(json_response)


def get_account_details(api_key):
    return _send_request(api_key, 'getAccountDetails')


def get_monitors(api_key):
    return _send_request(api_key, 'getMonitors', {'alert_contacts':
                                                  'true'})['monitors']


def add_monitor(api_key, config):
    return _send_request(api_key, 'newMonitor', config)


def delete_monitor(api_key, id):
    return _send_request(api_key, 'deleteMonitor', {'id': id})


def get_alert_contacts(api_key):
    return _send_request(api_key, 'getAlertContacts')['alert_contacts']


def add_alert_contact(api_key, config):
    return _send_request(api_key, 'newAlertContact', config)


def delete_alert_contact(api_key, id):
    return _send_request(api_key, 'deleteAlertContact', {'id': id})

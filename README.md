# uptimerobot-formula

A salt formula to manage [uptimerobot][1] monitors.

## Available states

* `alert_contacts_present`

  Define alert contacts to notify when monitors are down

* `monitors_present`

  Define endpoints to monitor, and which alert contacts to notify.

## Usage

This formula uses the uptimerobot API, and requires a API key. See [here][2] for
details.

```yaml
{% set api_key = 'xxxxxxxxxxxxxxxxxxxxxxxxxxx' %}

uptimerobot_alert_contacts:
  uptimerobot.alert_contacts_present:
    - api_key: {{ api_key }}
    - alert_contacts:
      - friendly_name: admin
        type: 2
        value: admin@example.com

uptimerobot_monitors:
  uptimerobot.monitors_present:
    - api_key: {{ api_key }}
    - monitors:
      - friendly_name: example
        url: https://www.example.com
        type: 2
        keyword_type: 2
        keyword_value: 'Example'
        alert_contacts:
          - admin
```

* Items are configured using the same parameters as in the [API docs][2] – for
  example, the above `type: 2` indicates a keyword-type monitor.

* The only exception to this is that alert contacts are identified using their
  "friendly name" rather than the internal ID; this is to allow the two states
  to be run independently. Note this however means that the friendly name is
  implicitly unique.

[1]: https://uptimerobot.com
[2]: https://uptimerobot.com/api
